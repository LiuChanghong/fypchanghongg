var configDB = require("./configDB");
var database = require("./database");

var Asset = database.Asset;
var Enquiry = database.Enquiry;
var Category = database.Category;
var Profile = database.Profile;
module.exports = function () {
  if (configDB.seed) {
    Asset.bulkCreate([
      {
        productID: "1",
        name: "Zephyrus M",
        description:
          "Thin and Portable Gaming Laptop, 15.6” 240Hz FHD IPS, NVIDIA GeForce RTX 2070, Intel Core i7-9750H, 16GB DDR4 RAM, 1TB PCIe SSD, Per-Key RGB, Windows 10 Home, GU502GW-AH76",
        brand: "ROG",
        price: "$1,849.99",
        categoryID: "1",
        imageURL: "aurora1.jpg",
        color:"Red",
        tag:"Headphone",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
      {
        productID: "2",
        name: "ZenBook Pro",
        description:
          "Duo UX581 15.6” 4K UHD NanoEdge Bezel Touch, Intel Core i7-9750H, 16GB RAM, 1TB PCIe SSD, GeForce RTX 2060, Innovative Screenpad Plus, Windows 10 Pro - UX581GV-XB74T, Celestial Blue",
        brand: "ASUS",
        price: "$2,378.21",
        categoryID: "1",
        imageURL: "aurora2.jpg",
        color:"Red",
        tag:"Headphone",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
      {
        productID: "3",
        name: "VivoBook S",
        description:
          "15.6in Full HD Laptop, Intel Core i7-8550U, NVIDIA GeForce MX150, 8GB RAM, 256GB SSD + 1TB HDD, Windows 10 (Renewed)",
        brand: "ASUS",
        price: "$749.66",
        categoryID: "1",
        imageURL: "aurora3.jpg",
        color:"Red",
        tag:"Headphone",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
      {
        productID: "4",
        name: "M17",
        description:
          "8th Gen Intel Core i7-8750H 6-Core | 17.3 Inch FHD 1920x1080 60Hz IPS | 16GB 2666MHz DDR4 RAM | 512GB SSD| NVIDIA GeForce RTX 2070 Max Q",
        brand: "Alienware",
        price: "$1,499.95",
        categoryID: "1",
        imageURL: "aurora4.jpg",
        color:"Red",
        tag:"Headphone",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
      {
        productID: "5",
        name: "ThinkPad P71",
        description:
          'Windows 10 Pro - Xeon E3-1535M, 64GB ECC RAM, 4TB SSD, 17.3" UHD 4K 3840x2160 Display, Quadro P4000 8GB GPU, Color Sensor, 4G LTE WWAN',
        brand: "Lenovo",
        price: "$6,049.12",
        categoryID: "1",
        imageURL: "aurora5.jpg",
        color:"Red",
        tag:"Headphone",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
      {
        productID: "6",
        name: "IPhone 11 Pro",
        description:
          "6. 5-inch Super Retina XDR OLED display, Water and dust resistant, Triple-camera system with 12MP Ultra wide, wide, and telephoto cameras; night mode, Portrait mode",
        brand: "Apple",
        price: "$1,099.00",
        categoryID: "2",
        imageURL: "Iphone11promax.jpg",
        color:"Red",
        tag:"Headphone",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
      {
        productID: "7",
        name: "Redmi Note 8",
        description:
          '64GB + 4GB RAM, 6.3" LTE 48MP Factory Unlocked GSM Smartphone - International Version (Moonlight White) 2MP - Front Camera: 13 MP, f/2.0 - Video: 2160p@30fps, 1080p@30/60/120fps, 720p@960fps',
        brand: "Xiaomi",
        price: "$174.50",
        categoryID: "2",
        imageURL: "redmi.jpg",
        color:"Red",
        tag:"Headphone",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
      {
        productID: "8",
        name: "MI Mix 3",
        description:
          '6.39" Display, Dual SIM 4G LTE GSM Unlocked Multi-Functional Magnetic Slider Smartphone w/Wireless Charging Pad (Black) AI music pairing for instant audiovisual beauty, 24MP + 2MP-front camera',
        brand: "Xiaomi",
        price: "$519.00",
        categoryID: "2",
        imageURL: "mimix.jpg",
        color:"Red",
        tag:"Headphone",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
      {
        productID: "9",
        name: "Mate 10 Pro",
        description:
          '6" 6GB/128GB, AI Processor, Dual Leica Camera, Water Resistant IP67, GSM Only - Titanium Gray With a large 4000 mAh battery coupled with smart battery management that learns from user behaviors',
        brand: "Huawei",
        price: "$479.99",
        categoryID: "2",
        imageURL: "mate10.jpg",
        color:"Red",
        tag:"Headphone",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
      {
        productID: "10",
        name: "Galaxy S110",
        description:
          "An immersive Cinematic Infinity Display, Pro grade Camera and Wireless PowerShare The next generation is here, Intelligently accesses power by learning how and when you use your phone.",
        brand: "Samsung",
        price: "$849.99",
        categoryID: "2",
        imageURL: "s10.jpg",
        color:"Red",
        tag:"Headphone",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
      {
        productID: "11",
        name: "AirPods Pro",
        description:
          "Active noise cancellation for immersive sound, Transparency mode for hearing and connecting with the world around you, Three sizes of soft, tapered silicone tips for a customizable fit",
        brand: "Apple",
        price: "$199.99",
        categoryID: "3",
        imageURL: "airpodspro.jpg",
        color:"Red",
        tag:"Headphone",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
      {
        productID: "12",
        name: "Mi AirDots",
        description:
          "Wireless Headphones Bluetooth V5.0 True Wireless Stereo Wireless Earphones with Wirelss Charging Case 12Hours Battery Life (Redmi Airdots), Bluetooth v5.0",
        brand: "Xiaomi",
        price: "$30.98",
        categoryID: "3",
        imageURL: "airdots.jpg",
        color:"Red",
        tag:"Headphone",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
      {
        productID: "13",
        name: "Powerbeats 3",
        description:
          "Connectivity Technology: Wireless Connect via Class 1 Bluetooth with your device for wireless workout freedom, Up to 12 hours of battery life to power through multiple workouts",
        brand: "Beats",
        price: "$79.99",
        categoryID: "3",
        imageURL: "powerbeats.jpg",
        color:"Red",
        tag:"nothing",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
      {
        productID: "14",
        name: "Galaxy Buds",
        description:
          "Bluetooth True Wireless Earbuds (Wireless Charging Case Included), Black - International Version, No Warranty， Premium sound Tuned by AKG",
        brand: "Samsung",
        price: "$149.99",
        categoryID: "3",
        imageURL: "galaxybuds.jpg",
        color:"Red",
        tag:"nothing",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
      {
        productID: "15",
        name: "AirPods",
        description:
          "Automatically on, automatically connected, asy setup for all your Apple devices, Double-tap to play or skip forward, Charges quickly in the case",
        brand: "Apple",
        price: "$125.99",
        categoryID: "3",
        imageURL: "airpods.jpg",
        color:"Red",
        tag:"Headphone",
        filetype:"image/png",
        view: "0",
        download: "0",
        assetscore: "0"
      },
    ]).then(function () {
      console.log("Done creating Asset records");

      Enquiry.bulkCreate([
        /* {enquiryusername:'1',enquiryemail:'adasd@enquiry.com',enquirysubject:'cantnot',enquirymessage:'my awydi ahsbduabwudbuoahd',enquirystatus:'unread'},
         */
      ])
        .then(function () {
          console.log("done creating Enquiry records");

          Category.bulkCreate([
            {
              categoryID: "1",
              categoryname: "Laptop",
              categorydescription: "Some sort of portable computer.",
            },
            {
              categoryID: "2",
              categoryname: "Telephones",
              categorydescription: "A portable communication tool.",
            },
            {
              categoryID: "3",
              categoryname: "Earphones",
              categorydescription:
                "A portable speaker where you can put in your year.",
            },
          ]).then(function () {
            console.log("done creating Category records");
          });
        })
        .then(function () {
          Profile.bulkCreate([
            {
              profileID: "1",
              logo: "logo.png",
              background: "background.png",
              brand: "Louken",
              slogan: "Branding Forward",
            },
          ]).then(function () {
            console.log("done creating Profile records");
          });
        });
    });
  }
};
