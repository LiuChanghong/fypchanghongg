const fs = require("fs");

var AssetList = [
  {
    productID: "1",
    name: "Zephyrus M",
    description:
      "Thin and Portable Gaming Laptop, 15.6” 240Hz FHD IPS, NVIDIA GeForce RTX 2070, Intel Core i7-9750H, 16GB DDR4 RAM, 1TB PCIe SSD, Per-Key RGB, Windows 10 Home, GU502GW-AH76",
    brand: "ROG",
    price: "$1,849.99",
    catgoryID: "1",
    imageURL: "./assets/img/ZephyrusM.jpg",
    view: "0",
    download: "0",
    assetscore: "0",
  },
  {
    productID: "2",
    name: "ZenBook Pro",
    description:
      "Duo UX581 15.6” 4K UHD NanoEdge Bezel Touch, Intel Core i7-9750H, 16GB RAM, 1TB PCIe SSD, GeForce RTX 2060, Innovative Screenpad Plus, Windows 10 Pro - UX581GV-XB74T, Celestial Blue",
    brand: "ASUS",
    price: "$2,378.21",
    catgoryID: "1",
    imageURL: "./assets/img/Zenbook.jpg",
    view: "0",
    download: "0",
    assetscore: "0",
  },
  {
    productID: "3",
    name: "VivoBook S",
    description:
      "15.6in Full HD Laptop, Intel Core i7-8550U, NVIDIA GeForce MX150, 8GB RAM, 256GB SSD + 1TB HDD, Windows 10 (Renewed)",
    brand: "ASUS",
    price: "$749.66",
    catgoryID: "1",
    imageURL: "./assets/img/vivobooks.jpg",
    view: "0",
    download: "0",
    assetscore: "0",
  },
];
var Asset = require("../../database").Asset;

var Profile = require("../../database").Profile;

var current_productID = 5;

exports.getProfile = function (req, resp) {
  Profile.findAll()
    .then((profile) => {
      resp.status(200).type("application/json").json(profile);
    })
    .catch((error) => {
      resp.status(500).type("application/json").json({ error: true });
    });
};

exports.list = function (req, resp) {
  Asset.findAll()
    .then((allAssetRecords) => {
      resp.status(200).type("application/json").json(allAssetRecords);
    })
    .catch((error) => {
      resp.status(500).type("application/json").json({ error: true });
    });
};
/*
  exports.list = function(req, resp){

    resp.status(200);
    resp.type("application/json");
    resp.json(AssetList);

  }*/
/*
  exports.add = function(req, resp) {
    if (!req.body.info) {
         handleErr(resp);
         resp.status(400).send("error bad request");
     } else {
        var newinfo = req.body.info;
        newinfo.productID = current_productID++;

        AssetList.push(newinfo);
        resp.status(200).type("application/json").json(newinfo);
     
    }
}*/
exports.addAsset = function (req, resp) {
  let base64String = req.body.info3;
  let base64Image = base64String.split(";base64,").pop();

  fs.writeFile(
    "../uploads/" + req.body.info4,
    base64Image,
    { encoding: "base64" },
    function (err) {
      console.log("File created");
      console.log(err);
      Asset.create({
        name: req.body.info,
        description: req.body.info2,
        brand: "brand",
        price: "$1",
        categoryID: req.body.info5,
        imageURL: req.body.info4,
        tag: req.body.tag,
        color:req.body.color,
        filetype:req.body.filetype,
        view: 0,
        download: 0,
        assetscore: 0

      })
        .then((newRecord) => {
          resp.status(200).type("application/json").json(newRecord);
        })
        .catch((error) => {
          resp.status(500).type("application/json").json({ error: true });
        });
    }
  );

  resp.status(500).type("application/json").json({ error: true });
};

exports.editProfile = function (req, resp) {
  let base64Stringlogo = req.body.logo;
  let base64Imagelogo = base64Stringlogo.split(";base64,").pop();

  let base64Stringbg = req.body.background;
  let base64Imagebg = base64Stringbg.split(";base64,").pop();
  var brand = req.body.brand;
  var slogan = req.body.slogan;
  var logoname = req.body.logoname;
  var backgroundname = req.body.backgroundname;
  fs.writeFile(
    "../uploads/" + req.body.logoname,
    base64Imagelogo,
    { encoding: "base64" },
    function (err) {
      console.log(err);
      console.log("File created");

      fs.writeFile(
        "../uploads/" + req.body.backgroundname,
        base64Imagebg,
        { encoding: "base64" },
        function (err) {
          console.log(err);
          console.log("File created");

          Profile.upsert({
            profileID: 1,
            logo: logoname,
            background: backgroundname,
            brand: brand,
            slogan: slogan,
          }).then(function (result) {
            resp.status(200).type("application/json").json({ success: true });
          });
        }
      );
    }
  );
};

exports.add = function (req, resp) {
  if (!req.body.info) {
    console.log("No info Found!");
    resp.status(400).json({ error: true });
  } else {
    var newAssetInfo = req.body.info;

    console.log(newAssetInfo);
    delete newAssetInfo["productID"];
    console.log(newAssetInfo);

    Asset.create(newAssetInfo)
      .then((newRecord) => {
        resp.status(200).type("application/json").json(newRecord);
      })
      .catch((error) => {
        resp.status(500).type("application/json").json({ error: true });
      });
  }
};
/*
exports.remove = function(req,resp){
  if(!req.params.productID){
       resp.status(400).send("Bad Request!");
  } else{
      var found = false;
      var productID = req.params.productID;
      for ( var i=0; i< AssetList.length; i++ ){
        if(AssetList[i].productID == productID){
          found = true;
          AssetList.splice(i,1);
          break;
        }
      }
      resp.status(200).type("application/json").json({success: found});
  }
}*/

exports.remove = function (req, resp) {
  if (!req.params.productID) {
    resp.status(400).send("Bad Request");
  } else {
    var whereClause = { productID: parseInt(req.params.productID) };

    Asset.destroy({ where: whereClause })
      .then((numOfRowDeleted) => {
        let status = numOfRowDeleted == 1;
        resp.status(200).type("application/json").json({ success: status });
      })
      .catch((error) => {
        resp.status(500).type("application/json").json({ error: true });
      });
  }
};

exports.update = function (req, resp) {
  if (!req.body.info) {
    resp.status(400).send("Bad Request");
  } else {
    var updateInfo = req.body.info;
    var whereClause = { productID: parseInt(req.params.productID) };

    Asset.update(updateInfo, { where: whereClause })
      .then((result) => {
        let status = result[0] == 1;

        resp.status(200).type("application/json").json({ success: status });
      })
      .catch((error) => {
        resp.status(500).type("application/json").json({ error: true });
      });
  }
};

function handleErr(res) {
  handleErr(res, null);
}

function handleErr(res, err) {
  console.log(err);
  res.status(500).json({ error: true });
}
