var Category = require("../../database").Category;

exports.addAssetCategory = function (req, resp) {
  console.log("INFO IS");
  console.log(req.body.info);

  Category.create({
    categoryname: req.body.info,
    categorydescription: "Some Category Description",
  })
    .then((newRecord) => {
      console.log(newRecord);
      // resp.status(200).type("application/json").json(newRecord);
    })
    .catch((error) => {
      console.log(error);
      // resp.status(500).type("application/json").json({ error: true });
    });

  resp.status(500).type("application/json").json({ error: true });
};

exports.list = function (req, resp) {
  Category.findAll()
    .then((allCategoryRecords) => {
      resp.status(200).type("application/json").json(allCategoryRecords);
    })
    .catch((error) => {
      resp.status(500).type("application/json").json({ error: true });
    });
};

function handleErr(res) {
  handleErr(res, null);
}

function handleErr(res, err) {
  console.log(err);
  res.status(500).json({ error: true });
}
