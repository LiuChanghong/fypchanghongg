
  var Enquiry = require('../../database').Enquiry;


exports.list = function (req, resp) {
  Enquiry.findAll(
      
  ).then(
      (allEnquiryRecords) => { 
          resp
              .status(200)
              .type('application/json')
              .json(allEnquiryRecords); 
      }
  ).catch(
      (error) => { 
          resp
              .status(500)
              .type('application/json')
              .json({error: true}) 
      }
  );
}

exports.add = function (req, resp) {
  if (!req.body.info) {
      console.log("No info Found!");
      resp.status(400).json({error: true});
  } else {
      var newEnquiryInfo = req.body.info;

     console.log(newEnquiryInfo);
      delete newEnquiryInfo['enquiryusername'];
     console.log(newEnquiryInfo);

      Enquiry.create(
        newEnquiryInfo
      ).then((newRecord) => {
          resp
              .status(200)
              .type('application/json')
              .json(newRecord);
      }).catch((error)=> {
          resp
              .status(500)
              .type('application/json')
              .json({error: true});
      });
  }
}


exports.remove = function (req, resp) {
  if (!req.params.enquiryusername) {
      resp.status(400).send("Bad Request");
  } else {
      var whereClause = {enquiryusername: parseInt(req.params.enquiryusername)};

      Enquiry.destroy(
          {where: whereClause}
      ).then((numOfRowDeleted) => {
          let status = (numOfRowDeleted == 1);
          resp
              .status(200)
              .type('application/json')
              .json({success: status});
      }).catch((error)=> {
          resp
              .status(500)
              .type('application/json')
              .json({error: true});
      });
  }
}

exports.update = function (req, resp) {
  if (!req.body.info) {
      resp.status(400).send("Bad Request");
  } else {
      var updateInfo = req.body.info;
      var whereClause = {enquiryusername: parseInt(req.params.enquiryusername)};

      Enquiry.update(
          updateInfo,
          {where: whereClause}
      ).then((result) => {
          let status = (result[0] == 1);

          resp
              .status(200)
              .type("application/json")
              .json({success: status});
      }).catch((error)=> {
          resp
              .status(500)
              .type('application/json')
              .json({error: true});
      });
  }
}

function handleErr(res) {
  handleErr(res, null);
}

function handleErr(res, err) {
  console.log(err);
  res.status(500).json({error: true});
}

