var Sequelize = require("sequelize");
var configDB = require("./configDB");

var database;

database = new Sequelize(
  configDB.mysql.database,
  configDB.mysql.username,
  configDB.mysql.password,
  {
    host: configDB.mysql.host,
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      idle: 10000,
    },
    logging: configDB.mysql.logging,
  }
);

var Asset = require("./models/Asset.model")(database);
var Enquiry = require("./models/enquiry.model")(database);
var Category = require("./models/category.model")(database);
var Profile = require("./models/profile.model")(database);

database
  .sync({
    force: configDB.seed,
  })
  .then(function () {
    require("./seed")();
    console.log("Seeding Database");
  });

module.exports = {
  Asset: Asset,
  Enquiry: Enquiry,
  Category: Category,
  Profile: Profile,
};
