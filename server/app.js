var express = require("express");
var bodyParser = require("body-parser");
var path = require("path");
var cors = require("cors");

var AssetCtrl = require("./api/Asset/Asset.controller");
var enquiryCtrl = require("./api/Enquiry/Enquiry.controller");
var categoryCtrl = require("./api/Category/Category.controller");

var app = express();

app.use(
  bodyParser.json({
    extended: true,
    limit: "50mb",
  })
);
app.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));
app.use(cors());

app.use(express.static(path.join(__dirname, "../public")));

app.get("/api/Asset", AssetCtrl.list);
app.post("/api/Asset", AssetCtrl.add);
app.post("/api/NewAsset", AssetCtrl.addAsset);

app.delete("/api/Asset/:productID", AssetCtrl.remove);
app.put("/api/Asset/:productID", AssetCtrl.update);

app.get("/api/Enquiry", enquiryCtrl.list);
app.post("/api/Enquiry", enquiryCtrl.add);
app.delete("/api/enquiry/:enquiryusername", enquiryCtrl.remove);
app.put("/api/enquiry/:enquiryusername", enquiryCtrl.update);

app.get("/api/Category", categoryCtrl.list);
app.post("/api/NewAssetCategory", categoryCtrl.addAssetCategory);

app.get("/api/profile", AssetCtrl.getProfile);
app.post("/api/editprofile", AssetCtrl.editProfile);

app.get("/imagefile/:imagename", (req, res) => {
  res.sendFile(path.join(__dirname, "../uploads/" + req.params.imagename));
});

app.get("/downloadfile/:imagename", (req, res) => {
  res.download(path.join(__dirname, "../uploads/" + req.params.imagename));
});

app.use(function (req, resp) {
  resp.status(404);
  resp.send("Error file not found");
});

app.listen("3000", function () {
  console.log("Server running at http://localhost:3000");
});
