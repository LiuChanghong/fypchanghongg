var Sequelize = require('sequelize');

module.exports = function (database) {
    return database.define('category', {
        categoryID : {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        categoryname : {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        categorydescription : {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        }

    }, {
        tableName: 'Category',
        timestamps: false
    });
}
