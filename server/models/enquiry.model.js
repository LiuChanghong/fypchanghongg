var Sequelize = require('sequelize');

module.exports = function (database) {
    return database.define('enquiry', {
        enquiryusername : {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        enquiryemail : {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        enquirysubject : {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        enquirymessage: {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        enquirystatus: {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        }

    }, {
        tableName: 'Enquiry',
        timestamps: false
    });
}
