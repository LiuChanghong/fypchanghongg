var Sequelize = require('sequelize');

module.exports = function (database) {
    return database.define('Asset', {
        productID : {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        name : {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        description : {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        brand : {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        }
        ,
        price : {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        }
        ,
        categoryID : {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        }
        ,
        imageURL : {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        }
        ,
        color : {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        }  ,
        tag : {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        filetype : {
            type: Sequelize.STRING(1000),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        view : {
            type: Sequelize.INTEGER,
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        download : {
            type: Sequelize.INTEGER,
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        assetscore : {
            type: Sequelize.INTEGER,
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },

        
    }, {
        tableName: 'Asset',
        timestamps: false
    });
}
