var Sequelize = require("sequelize");

module.exports = function (database) {
  return database.define(
    "profile",
    {
      profileID: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      logo: {
        type: Sequelize.STRING(1000),
        primaryKey: false,
        autoIncrement: false,
        allowNull: false,
      },
      background: {
        type: Sequelize.STRING(1000),
        primaryKey: false,
        autoIncrement: false,
        allowNull: false,
      },

      brand: {
        type: Sequelize.STRING(1000),
        primaryKey: false,
        autoIncrement: false,
        allowNull: false,
      },

      slogan: {
        type: Sequelize.STRING(1000),
        primaryKey: false,
        autoIncrement: false,
        allowNull: false,
      },
    },
    {
      tableName: "Profile",
      timestamps: false,
    }
  );
};
